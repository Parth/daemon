// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

import (
	"io/ioutil"

	"gopkg.in/yaml.v3"
)

type ProjectConfig struct {
	TopLevelDomain    string `yaml:"domain"`
	MasterSubdomain   string `yaml:"masterSubdomain"` // Defaults to none.
	ExtraCaddyConfigs string `yaml:"caddyConfigs"`

	Build struct {
		Cmds []string `yaml:"cmds"`
		Dir  string   `yaml:"dir"`
	} `yaml:"build"`
}

func ReadProjectConfig(filePath string) (config *ProjectConfig, err error) {
	yamlData, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	if err = yaml.Unmarshal(yamlData, &config); err != nil {
		return nil, err
	}

	return config, nil
}
