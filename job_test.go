// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

import (
	"encoding/json"
	"testing"
)

// Test the IsClosingPush method of Wehbook.
func TestIsClosingPush(t *testing.T) {
	// Test data consisting of JSON text with the fields that IsClosingPush
	// looks at as well as whether we expect it to be a closing push or not.
	testData := []struct {
		jsonText      string
		isClosingPush bool
	}{
		{`{"after": "0000000000000000000000000000000000000000", "checkout_sha": null}`, true},
		{`{"after": "4de4a2ba5521667299d28a2281eb168b8ac5981f", "checkout_sha": "4de4a2ba5521667299d28a2281eb168b8ac5981f"}`, false},
	}

	for _, td := range testData {
		// Unmarshal our test data to a Job object.
		var j Job
		if err := json.Unmarshal([]byte(td.jsonText), &j); err != nil {
			panic(err)
		}

		// Make sure IsClosingPush returns expected value.
		if j.IsClosingPush() != td.isClosingPush {
			t.Errorf("expected IsClosingPush to be %v. checkout_sha %v, after: %v", td.isClosingPush, j.CheckoutSha, j.After)
		}
	}
}

// Test the IsClosingPush method of Wehbook.
func TestBranchName(t *testing.T) {
	testJob := Job{
		Ref: "refs/heads/branch1",
	}

	if testJob.BranchName() != "branch1" {
		t.Errorf("Job.BranchName(): wanted 'branch1', got '%v'", testJob.BranchName())
	}
}
