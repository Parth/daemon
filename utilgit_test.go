// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

import (
	"testing"
)

func TestGitIsValidHash(t *testing.T) {
	testHash := "961a0483603d016b159b728519e9a4951351356c"
	if !gitHashIsValid(".", testHash) {
		t.Error("expected hash to be valid")
	}
}
