// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"
)

func main() {
	// Initialize program configuration and process command line arguments.
	config := NewConfiguration()
	if err := config.ParseCliArgs(); err != nil {
		log.Fatalf("devopsd: error: %v\n%v\n", err, config.Usage())
	}

	// Read in configuration settings from the configuration file.
	if err := config.LoadConfigFile(); err != nil {
		log.Fatalf("devopsd: error: %v", err)
	}

	// Open our database.
	dbFile := filepath.Join(config.Get("datadir"), "devopsd.db")
	db, err := OpenNewDatabase(dbFile)
	if err != nil {
		log.Fatalf("devopsd: error: %v", err)
	}

	// Initialize and start our RunnerManager.
	runMngr := NewRunnerManager(config)
	runMngr.Start(db)

	// Initialize the environment.
	env := &Environment{Config: config, Db: db, RunMngr: runMngr}

	// Initialize the HTTP server.
	srv := &http.Server{
		Addr:         fmt.Sprintf("%v:%v", config.Get("host"), config.Get("port")),
		WriteTimeout: time.Second * 15, // Good practice to set timeouts
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      NewDevopsdRouter(env), // Our gorilla/mux router
	}

	// Listening in goroutine will prevent the HTTP server from blocking.
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Print(err)
		}
	}()

	// Defer resource cleanup.
	defer func() {
		// Shut down the HTTP server.
		if err := srv.Shutdown(context.Background()); err != nil {
			log.Printf("error shutting down server: %v", err)
		}

		// Stop the RunnerManager.
		runMngr.Stop()

		// Close the database.
		if err := db.Close(); err != nil {
			log.Printf("error closing database: %v", err)
		}
	}()

	// Channel for receiving OS signals.
	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh)

	// Continually receive OS signals.
	for {
		switch <-sigCh {
		case syscall.SIGINT:
			log.Println("devopsd: shutting down")
			return
		}
	}
}
