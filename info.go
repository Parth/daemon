// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

type OverviewInfo struct {
	RunnerCount int   `json:"runner_count"`
	Jobs        []Job `json:"jobs"`
}
