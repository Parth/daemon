#!/bin/sh
##
## Install askad on FreeBSD system.
##

# Misc configs.
: ${prefix="/usr/local"}
: ${name="devopsd"}

# Destination paths.
: ${dest_bin="${prefix}/bin"}
: ${dest_conf="${prefix}/etc/${name}.conf"}
: ${dest_rc_d="${prefix}/etc/rc.d/${name}"}

# Project resources.
: ${proj_bin="./${name}"}
: ${proj_conf="./testdata/${name}.conf"}
: ${proj_rc_d="./contrib/init/${name}.rc.d"}

# Copy the rc.d script and set the rc enable var.
sudo cp ${proj_rc_d} ${dest_rc_d}
sudo sysrc ${name}_enable=YES

# Compile.
go build || exit 2

# Stop devopsd if it's running.
sudo service devopsd stop

# Copy the binary.
sudo cp ${proj_bin} ${dest_bin} || exit 3

# Copy the configuration if there isn't one already in place.
# If there is, then start devopsd.
if [ ! -e "${dest_conf}" ]; then
	sudo cp ${proj_conf} ${dest_conf}
	echo "Copied sample config, so go edit ${dest_conf}"
else
	sudo service devopsd start
fi
