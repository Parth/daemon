#!/bin/sh

. /etc/rc.subr

name="devopsd"
rcvar="${name}_enable"

load_rc_config ${name}

: ${devopsd_enable:=NO}
: ${devopsd_config_path:="/usr/local/etc/${name}.conf"}
: ${devopsd_logfile="/var/log/${name}.log"}
: ${devopsd_user="root"}
: ${devopsd_group="wheel"}

pidfile="/var/run/${name}.pid"
procname="/usr/local/bin/${name}"

command="/usr/sbin/daemon"
command_args="-p ${pidfile} -o ${devopsd_logfile} -u ${devopsd_user} ${procname} -conf=${devopsd_config_path} -printconf"

start_precmd="devopsd_startprecmd"

devopsd_startprecmd()
{
	if [ ! -e "${pidfile}" ]; then
		install -o "${devopsd_user}" -g "${devopsd_group}" "/dev/null" "${pidfile}"
	fi

	if [ ! -e "${devopsd_logfile}" ]; then
		install -o "${devopsd_user}" -g "${devopsd_group}" "/dev/null" "${devopsd_logfile}"
	fi
}

required_files="${devopsd_config_path}"

run_rc_command "$1"
