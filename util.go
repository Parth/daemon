// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

func CommandStr(cmdStr string) *exec.Cmd {
	return exec.Command("sh", "-c", cmdStr)
}

func Commandf(format string, a ...interface{}) (*exec.Cmd, string) {
	cmdStr := fmt.Sprintf(format, a...)
	return CommandStr(cmdStr), cmdStr
}

// FullDomain returns the full domain name of the deploy being built given
// the branch name and the ProjectConfig in the repo.
func FullDomain(branch string, pc *ProjectConfig) string {
	// If we're building master, and there is no name for the subdomain,
	// then just use the top level domain (master -> domain.com)
	// If we're building master, and there is a name for the subdomain,
	// then use that configuration value (master -> mastersub.domain.com)
	if branch == "master" {
		if pc.MasterSubdomain != "" {
			return pc.MasterSubdomain + "." + pc.TopLevelDomain
		} else {
			return pc.TopLevelDomain
		}
	}

	// If we're building any branch other than master, then that branch's
	// name is used as the subdomain.
	return branch + "." + pc.TopLevelDomain
}

func Process(j *Job, caddyRoot string) error {
	// Clone the project into a temporary directory.
	projDir, err := gitCloneRepo("", j.RepoUrl)
	if err != nil {
		return err
	}

	// Defer cleanup. After the call to Process returns, remove the tmp dir.
	defer func() {
		if err := os.RemoveAll(projDir); err != nil {
			log.Printf("process cleanup: rm %v: ", err)
		}
	}()

	// Checkout the appropriate commit hash.
	if err := gitCheckout(projDir, j.TargetCommitHash()); err != nil {
		return err
	}

	// Read project `devopsd.yml` config file.
	projConf, err := ReadProjectConfig(filepath.Join(projDir, ".devopsd.yml"))
	if err != nil {
		return fmt.Errorf("read project config: %v", err)
	}

	// If the webhook was triggered from a branch being deleted,
	// then we can remove the deploy setup for that branch.
	if j.IsClosingPush() {
		return removeDeploy(j, projConf, caddyRoot)
	}

	// Default to building and deploying.
	return BuildAndDeploy(j, projDir, projConf, caddyRoot)
}

// removeDeploy deletes the deploy directory and Caddyfile for the given project info.
func removeDeploy(j *Job, projConf *ProjectConfig, caddyRoot string) error {
	fullDomain := FullDomain(j.BranchName(), projConf)
	targetDir := filepath.Join(caddyRoot, fullDomain)
	caddyFile := targetDir + ".Caddyfile"

	// Log this call to remove a deploy.
	log.Printf("removing %v", fullDomain)

	// Try to remove the project's deploy directory.
	if err := os.RemoveAll(targetDir); err != nil {
		return err
	}

	// Try to remove the project's Caddyfile.
	if err := os.Remove(caddyFile); err != nil {
		return err
	}

	// Reload Caddy.
	if err := ReloadCaddy(); err != nil {
		return err
	}

	// All went well, deployed directory and Caddyfile are gone.
	return nil
}

// BuildAndDeploy builds the project in the `projDir` by running the build commands
// found in `projConf`. Then, it deploys the built project to the deploy location,
// a uniquely named directory in `caddyRoot`, as well as creates a Caddyfile for
// this new deploy. Lastly, it reloads Caddy so the new deploy takes affect.
func BuildAndDeploy(j *Job, projDir string, projConf *ProjectConfig, caddyRoot string) error {
	// Full domain name of the site (eg: branch.domain.com),
	// the build directory, and the target deploy directory.
	fullDomain := FullDomain(j.BranchName(), projConf)
	sourceDir := filepath.Join(projDir, projConf.Build.Dir)
	targetDir := filepath.Join(caddyRoot, fullDomain)

	// Log deploy info whenever this function returns.
	defer func() {
		log.Printf("done with %v -> %v", projDir, targetDir)
	}()

	buildCmdsStr := strings.Join(projConf.Build.Cmds, " && ")
	buildCmds := CommandStr(buildCmdsStr)
	buildCmds.Dir = projDir
	buildCmds.Stdout = os.Stdout
	buildCmds.Stderr = os.Stderr
	if err := buildCmds.Run(); err != nil {
		return fmt.Errorf("'%v': %v: %v", projDir, buildCmdsStr, err)
	}

	// Clear the deploy location.
	if err := os.RemoveAll(targetDir); err != nil {
		return fmt.Errorf("rm %v: %v", targetDir, err)
	}

	// Move the build directory to its deploy location.
	deployCmd := exec.Command("mv", sourceDir, targetDir)
	if err := deployCmd.Run(); err != nil {
		return fmt.Errorf("mv %v %v: %v", sourceDir, targetDir, err)
	}

	// Add Caddy config file. Use the path returned for a more detailed error message.
	if caddyFp, err := WriteCaddyFile(caddyRoot, fullDomain, projConf.ExtraCaddyConfigs); err != nil {
		return fmt.Errorf("caddyfile %v: %v", caddyFp, err)
	}

	// Reload Caddy, return the error.
	return ReloadCaddy()
}
