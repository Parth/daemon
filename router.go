// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

// mwSetRespHeaders sets the headers that will be uniform across all responses.
// Currently, since all responses are JSON, we can set the content type header.
func mwSetRespHeaders(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "application/json") // Any response will be JSON.

		next.ServeHTTP(w, req)
	})
}

// mwBasicChecks performs basic authentication and safety checks on the request data.
// Specifically, it first checks that the correct GitLab token header is set. Then,
// it makes sure the body isn't nil.
func mwBasicChecks(env *Environment) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
			// First, let's make sure this request is legitimate by verifying the secret token.
			if glToken := req.Header.Get("X-Gitlab-Token"); glToken != env.Config.Get("gitlab-token") {
				http.Error(w, `{"error": "gitlab token mismatch"}`, http.StatusBadRequest)
				return
			}

			// Next, to avoid nil pointers, let's make sure req.Body is not nil.
			if req.Body == nil {
				http.Error(w, `{"error": "body is nil"}`, http.StatusBadRequest)
				return
			}

			next.ServeHTTP(w, req)
		})
	}
}

// NewDevopsdRouter returns an instance of the `gorilla/mux` router that will
// serve the endpoints that handle the webhook triggers.
func NewDevopsdRouter(env *Environment) *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/glwh-deploy", GlwhBuildAndDeploy(env)).Methods("POST")
	router.HandleFunc("/info-overview", InfoOverview(env)).Methods("GET")
	router.Use(mwSetRespHeaders)
	router.Use(mwBasicChecks(env))
	return router
}
