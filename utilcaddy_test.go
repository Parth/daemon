// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
)

// Test the FullDomain function by testing different project configurations
// and branch names.
func TestWriteCaddyFile(t *testing.T) {
	testCaddyRoot := "./testdata"
	testFullDomain := "branch1.domain.com"
	testSiteRoot := filepath.Join(testCaddyRoot, testFullDomain)
	testExtras := "rewrite {\n\t\tregexp .* to {path} /\n\t}"
	expectedCaddyFileContent := fmt.Sprintf("\n%v {\n\troot %v\n\t%s\n}\n", testFullDomain, testSiteRoot, testExtras)

	// Test writing to a Caddyfile.
	testCaddyFp, err := WriteCaddyFile(testCaddyRoot, testFullDomain, testExtras)
	if err != nil {
		t.Errorf("error writing caddy file: %v", err)
	}

	// Defer removing the test Caddyfile.
	defer func() {
		if err := os.Remove(testCaddyFp); err != nil {
			t.Errorf("error removing caddy file %v: %v", testCaddyFp, err)
		}
	}()

	// Read in the contents from the Caddyfile we just wrote to.
	caddyFileData, err := ioutil.ReadFile(testCaddyFp)
	if err != nil {
		t.Errorf("error reading caddy file: %v", err)
	}

	// Compare the content that we just read to what we expect.
	if string(caddyFileData) != expectedCaddyFileContent {
		t.Errorf("caddyfile: got %v, wanted %v", string(caddyFileData), expectedCaddyFileContent)
	}
}
