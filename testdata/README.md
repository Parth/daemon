# Testing

The code can of course be tested with:
```shell
go test
```

## Configuration Files

* `devopsd.conf` - an example of the **system** configuration file for `devopsd`
* `.devopsd.yml` - an example of the **project** configuration file that `devopsd` will read before building

## GitLab Webhook Request Data

The webhook JSON files contain the example POST request data from GitLab:
* [gitlab-webhook-push.json](https://gitlab.com/help/user/project/integrations/webhooks#push-events)
* gitlab-webhook-push.trimmed.json (only has the information that gets used by the program)

## Dummy Test Project

There is a dummy project (located [here](https://gitlab.com/steverusso/devopsd-dummy)) that is used for testing purposes.
It is also a good example of what `devopsd` does.

There are two branches: `master` and `branch1`.
The `.devopsd.yml` file in the repo has a `domain` configuration value of `devopsdummy.steverusso.tech`.
Therefore, the branches are deployed at the following locations:
* master -> https://www.devopsdummy.steverusso.tech/
* branch1 -> https://branch1.devopsdummy.steverusso.tech/
