#!/bin/sh

curl http://localhost:10101/glwh-deploy \
  -X POST \
  -H "Content-Type: text/json" \
  -H "X-Gitlab-Token: ffca05f9" \
  -H "X-Gitlab-Event: Push Hook" \
  --data-binary "@testdata/gitlab-webhook-push.trimmed.json" \
  --include # Include the HTTP response headers in the output.

