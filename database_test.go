// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

import (
	"encoding/json"
	"io/ioutil"
	"testing"
)

var (
	testDb   *Database
	webhooks []Webhook
)

// Read in test data.
func init() {
	jsonText, err := ioutil.ReadFile("./testdata/webhooks.json")
	if err != nil {
		panic(err)
	}

	if err = json.Unmarshal(jsonText, &webhooks); err != nil {
		panic(err)
	}
}

// jobMatchesWebhook is a testing utility function to determine if the webhook
// data from the given Job matches the data from the given Webhook.
func jobMatchesWebhook(j *Job, wh *Webhook) bool {
	return (j.Kind == wh.ObjectKind &&
		j.Before == wh.Before &&
		j.After == wh.After &&
		j.CheckoutSha == wh.CheckoutSha &&
		j.Ref == wh.Ref &&
		j.RepoUrl == wh.Project.GitHttpUrl)
}

// TestOpenJobDb tests opening a the database.
func TestOpenNewDatabase(t *testing.T) {
	var err error

	testDb, err = OpenNewDatabase("file:devopsd.testing.db?mode=memory")
	if err != nil {
		t.Error(err)
	}
}

// TestDbAddJob tests adding a job to the database.
func TestDbAddJob(t *testing.T) {
	// Add the job. Fail test if there was an error.
	jobId, err := testDb.AddJob(&webhooks[0])
	if err != nil {
		t.Error(err)
	}

	// Error if the first job added doesn't have an ID of 1.
	if jobId != 1 {
		t.Errorf("wanted jobId of 1, got %v", jobId)
	}
}

// TestDbGetUnreadJobs tests getting the unread jobs from the database.
func TestDbGetUnreadJobs(t *testing.T) {
	jobs, err := testDb.GetUnreadJobs()
	if err != nil {
		t.Error(err)
	}

	if len(jobs) <= 0 {
		t.Errorf("wanted to get some unread jobs, got %v", len(jobs))
	}

	if !jobMatchesWebhook(&jobs[0], &webhooks[0]) {
		t.Error("retrieved job from db does not match test data")
	}
}

// TestDbMarkJobDone tests getting the unread jobs from the database.
func TestDbMarkJobDone(t *testing.T) {
	err := testDb.MarkJobDone(1)
	if err != nil {
		t.Error(err)
	}

	job, err := testDb.GetJobById(1)
	if err != nil {
		t.Error(err)
	}

	if job.Id != 1 {
		t.Errorf("wanted job ID of 1, got %v", job.Id)
	}
	if job.Status != 2 {
		t.Errorf("wanted job status of 2, got %v", job.Status)
	}
}
