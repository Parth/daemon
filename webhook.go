// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

type Webhook struct {
	ObjectKind string `json:"object_kind"` // 'push', 'tag', etc.

	Before      string `json:"before"`
	After       string `json:"after"`
	CheckoutSha string `json:"checkout_sha"`
	Ref         string `json:"ref"`

	Project struct {
		GitHttpUrl string `json:"git_http_url"` // The URL we'll use to clone.
	} `json:"project"`
}
