// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

import (
	"database/sql"

	_ "github.com/mattn/go-sqlite3"
)

const (
	// The table that stores job information.
	//  - status: 0=unread, 1=queued, 2=completed
	QUERY_CreateTableJobs = `
		CREATE TABLE IF NOT EXISTS jobs (
		  id           INTEGER PRIMARY KEY,
			status       INTEGER DEFAULT 0,
			object_kind  TEXT,
		  before       TEXT,
		  after        TEXT,
		  checkout_sha TEXT,
			ref          TEXT,
		  git_http_url TEXT
	  )
	`

	// Query to insert job information into jobs table.
	QUERY_AddJob = `
		INSERT
		INTO jobs (object_kind, before, after, checkout_sha, ref, git_http_url)
		VALUES    (          ?,      ?,     ?,            ?,   ?,            ?)
	`

	// Query to retrieve the job info given its ID.
	QUERY_GetJobById = `
		SELECT *
		FROM   jobs
		WHERE  id = ?
	`

	// Query to retrieve all jobs with 'unread' status.
	QUERY_GetUnreadJobs = `
		SELECT *
		FROM   jobs
		WHERE  status = 0
	`

	// Query to mark the job with the given ID as read.
	QUERY_MarkJobRead = `
		UPDATE jobs
		SET    status = 1
		WHERE  id = ?
	`

	// Query to mark the job with the given ID as complete.
	QUERY_MarkJobDone = `
		UPDATE jobs
		SET    status = 2
		WHERE  id = ?
	`
)

type Database struct {
	*sql.DB
}

// OpenJobDb opens the SQLite database (given the path to the database)
// and creates the 'jobs' table if it doesn't already exist.
func OpenNewDatabase(pathToDb string) (*Database, error) {
	// Open the database.
	db, err := sql.Open("sqlite3", pathToDb)
	if err != nil {
		return nil, err
	}

	// Calling Ping will ensure the connection is alive.
	if err = db.Ping(); err != nil {
		return nil, err
	}

	// Execute the query to create the jobs table.
	if _, err = db.Exec(QUERY_CreateTableJobs); err != nil {
		return nil, err
	}

	return &Database{db}, nil
}

// DbAddJob adds the given job to the 'jobs' table in the database.
func (db *Database) AddJob(w *Webhook) (int64, error) {
	// Start a transaction.
	txn, err := db.Begin()
	if err != nil {
		return -1, err
	}

	// Execute the query to add a job with the given info from the webhook.
	res, err := txn.Exec(QUERY_AddJob,
		w.ObjectKind,
		w.Before,
		w.After,
		w.CheckoutSha,
		w.Ref,
		w.Project.GitHttpUrl,
	)
	if err != nil {
		return -1, err
	}

	// Get the jod ID and any error from the executed INSERT query.
	jobId, err := res.LastInsertId()
	if err != nil {
		return -1, err
	}

	// Commit the transaction. Return the job ID and no error.
	return jobId, txn.Commit()
}

// DbGetUnreadJobs first gets all jobs that have a status of 'unread.'
// Then it sets the status of those jobs to 'read.'
func (db *Database) GetUnreadJobs() (jobs []Job, err error) {
	// Start a transaction.
	txn, err := db.Begin()
	if err != nil {
		return nil, err
	}

	// Select the jobs that have an 'unread' status.
	rows, err := txn.Query(QUERY_GetUnreadJobs)
	if err != nil {
		return nil, err
	}

	// Loop over the rows of unread jobs.
	for rows.Next() {
		var j Job
		err := rows.Scan(
			&j.Id,
			&j.Status,
			&j.Kind,
			&j.Before,
			&j.After,
			&j.CheckoutSha,
			&j.Ref,
			&j.RepoUrl,
		)
		if err != nil {
			return nil, err
		}

		// Add the job to the return argument.
		jobs = append(jobs, j)
	}

	// Loop over the unread jobs we just retrieved.
	for _, j := range jobs {
		// Execute the query to mark each as read.
		if _, err = txn.Exec(QUERY_MarkJobRead, j.Id); err != nil {
			return nil, err
		}
	}

	// Return the slice of unread jobs and any error from committing the txn.
	return jobs, txn.Commit()
}

func (db *Database) MarkJobDone(jobId int64) error {
	// Start a transaction.
	txn, err := db.Begin()
	if err != nil {
		return err
	}

	// Execute the query to mark the given job as complete.
	if _, err = txn.Exec(QUERY_MarkJobDone, jobId); err != nil {
		return err
	}

	// Return any error from committing the txn.
	return txn.Commit()
}

// DbGetJobById retrieves the info the job with the given ID.
func (db *Database) GetJobById(jobId int64) (j Job, err error) {
	// Start a transaction.
	txn, err := db.Begin()
	if err != nil {
		return j, err
	}

	// Select the jobs that have an 'unread' status.
	row := txn.QueryRow(QUERY_GetJobById, jobId)
	if err != nil {
		return j, err
	}

	// Scan the row into the Job object.
	// If there was no job, err will be ErrNoRows.
	err = row.Scan(
		&j.Id,
		&j.Status,
		&j.Kind,
		&j.Before,
		&j.After,
		&j.CheckoutSha,
		&j.Ref,
		&j.RepoUrl,
	)
	if err != nil {
		return j, err
	}

	// Return the Job and any error from committing the txn.
	return j, txn.Commit()
}

func (db *Database) GetAllJobs() (jobs []Job, err error) {
	// Start a transaction.
	txn, err := db.Begin()
	if err != nil {
		return nil, err
	}

	// Query to select all jobs.
	rows, err := txn.Query("SELECT * FROM jobs")
	if err != nil {
		return nil, err
	}

	// Loop over the rows of jobs.
	for rows.Next() {
		var j Job
		err := rows.Scan(
			&j.Id,
			&j.Status,
			&j.Kind,
			&j.Before,
			&j.After,
			&j.CheckoutSha,
			&j.Ref,
			&j.RepoUrl,
		)
		if err != nil {
			return nil, err
		}

		// Add the job to the return argument.
		jobs = append(jobs, j)
	}

	return jobs, txn.Commit()
}
