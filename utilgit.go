// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
)

// cloneRepo creates a temporary directory and clones the given repoUrl to it.
func gitCloneRepo(options, repoUrl string) (projDir string, err error) {
	// Create a new temporary directory.
	if projDir, err = ioutil.TempDir("", "devopsd-"); err != nil {
		return "", fmt.Errorf("creating tmp dir: %v", err)
	}

	// Clone the repo into the temporary directory we just created.
	gitClone, cmdStr := Commandf("git clone %s %s %s", options, repoUrl, projDir)
	gitClone.Stdout = os.Stdout
	gitClone.Stderr = os.Stderr
	if err := gitClone.Run(); err != nil {
		return projDir, fmt.Errorf("%v: %v", cmdStr, err)
	}

	// All went well, return path to the tmp dir and nil for no errors.
	return projDir, nil
}

// checkoutAndGetConf checks out the given commit hash and reads
// the `.devopsd.yml` file to get the project configurations. It
// returns the ProjectConfig and any error.
func gitCheckout(projDir, commitHash string) error {
	// If the target hash is invalid, simply return without error.
	if !gitHashIsValid(projDir, commitHash) {
		return nil
	}

	// Checkout the given commitHash.
	gitCheckout := exec.Command("git", "checkout", commitHash, "-b", "tmpBranch")
	gitCheckout.Dir = projDir
	gitCheckout.Stderr = os.Stderr
	if err := gitCheckout.Run(); err != nil {
		return fmt.Errorf("git checkout: %v", err)
	}

	// All went well, return tmp dir, project conf, and nil for no errors.
	return nil
}

func gitHashIsValid(projDir, commitHash string) bool {
	cmd, _ := Commandf("git cat-file -t %s", commitHash)
	cmd.Dir = projDir
	output, err := cmd.CombinedOutput()
	return err == nil && string(output) == "commit\n"
}
