// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

import (
	"testing"
)

func TestReadProjectConfig(t *testing.T) {
	// Load values from test project configuration file.
	testProjConf, err := ReadProjectConfig("./testdata/.devopsd.yml")
	if err != nil {
		t.Error(err)
	}

	// Create configuration with the values we expect from the test file.
	expectedConf := &ProjectConfig{}
	expectedConf.TopLevelDomain = "steverusso.tech"
	expectedConf.Build.Cmds = []string{"yarn", "yarn gen"}
	expectedConf.Build.Dir = "dist"

	if testProjConf.TopLevelDomain != expectedConf.TopLevelDomain {
		t.Errorf("project-conf: wanted %v, got %v", testProjConf.TopLevelDomain, expectedConf.TopLevelDomain)
	}
	if testProjConf.Build.Dir != expectedConf.Build.Dir {
		t.Errorf("project-conf: wanted %v, got %v", testProjConf.Build.Dir, expectedConf.Build.Dir)
	}

	// Ensure the lists of build commands match.
	for i, cmd := range testProjConf.Build.Cmds {
		expCmd := expectedConf.Build.Cmds[i]
		if cmd != expCmd {
			t.Errorf("project-conf: wanted %v, got %v", cmd, expCmd)
		}
	}
}
