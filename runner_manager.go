// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

import (
	"log"
	"strconv"
	"time"
)

// A RunnerManager controls a group of Runners.
type RunnerManager struct {
	caddyRoot string

	// runners is the group of runners that the RunnerManager controls.
	runners []*Runner

	// jobCh is used by the RunnerManager to send jobs to its Runners.
	// The RunnerManager is the sender; the Runners are the receivers.
	jobCh chan *Job

	// stopCh is a signal channel for the
	// RunnerManager's own goroutines.
	stopCh chan struct{}
}

// NewRunnerManager is the constructor for RunnerManager. It creates the
// Job channel and signal channel, as well as the given amount of Runners.
// It does not start the Runners.
func NewRunnerManager(c *Configuration) *RunnerManager {
	// Create a new RunnerManager,
	rm := &RunnerManager{
		caddyRoot: c.Get("caddy-root"),
		jobCh:     make(chan *Job),
		stopCh:    make(chan struct{}),
	}

	runnerCount, err := strconv.Atoi(c.Get("runner-count"))
	if err != nil {
		runnerCount = 1
	}

	// Create the given number of runners.
	for i := 0; i < runnerCount; i++ {
		rm.runners = append(rm.runners, NewRunner(i))
	}

	return rm
}

// Start starts all of the RunnerManager's Runners
// and begins to poll the database for jobs.
func (rm *RunnerManager) Start(db *Database) {
	// Start all the Runners and provide them the channel to receive from.
	for _, r := range rm.runners {
		r.Start(rm.jobCh, rm.caddyRoot)
	}

	// Goroutine to poll database and send jobs.
	go func() {
		for {
			select {
			// Check if the signal channel is closed.
			// If so, return to end this goroutine.
			case _, closed := <-rm.stopCh:
				if closed {
					return
				}
			// While the signal channel is not closed, continue to
			// poll the database and send Jobs down the Job channel.
			default:
				// Get any unread jobs in database.
				jobs, err := db.GetUnreadJobs()
				if err != nil {
					log.Print(err)
				}

				// Send them down the jobs channel.
				for _, j := range jobs {
					rm.jobCh <- &j
				}

				// Wait 5 seconds before checking db again.
				time.Sleep(time.Second * 5)
			}
		}
	}()
}

// RunnerCount returns the current number of Runners.
func (rm *RunnerManager) RunnerCount() int {
	return len(rm.runners)
}

// Stop will stop all of the RunnerManager's Runners
// and close its own signal channel as well.
func (rm *RunnerManager) Stop() {
	// Stop the Runners.
	for _, r := range rm.runners {
		r.Stop()
	}

	// Close the signal channel.
	close(rm.stopCh)
}
