# Devopsd

Website branch deploys using Git and Caddy.

## Usage

### Setup

1. Create a wildcard record for the domain you wish to use pointed to the server that `devopsd` will be installed on.

2. Install and configure the dependencies (see above).
   * [Git](https://git-scm.com/) (Installing on: [FreeBSD](https://freebsd.sh/git/))
   * [Go](https://golang.org/) (Installing on: [FreeBSD](https://freebsd.sh/go/))
   * [Caddy](https://caddyserver.com/) (Installing on: [FreeBSD](https://freebsd.sh/caddy/))
     * For `devopsd`, there's just one more step.
		   We need to make sure the Caddyfile (located at `/usr/local/www/Caddyfile` by default) contains at least the following:
       ```
       devopsd.example.com {
           proxy / localhost:10101
       }

       import *.Caddyfile
       ```
       (Be sure to replace `example.com` with the domain you are using!)
     * **Note:** The `command_args` variable in the Caddy rc file should be changed to the following so that output from `USR1` signals shows up in logs:
       ```bash
       command_args="-p ${pidfile} -o ${caddy_logfile} /usr/bin/env ${caddy_env} ${procname} -cpu ${caddy_cpu} -log stdout -conf ${caddy_config_path} -agree -email ${caddy_cert_email}"
       ```

3. Install and configure `devopsd`:
   ```bash
	 git clone https://gitlab.com/steverussotech/devopsd.git
	 cd devopsd
	 ./contrib/init/install-freebsd.sh
	 sudo service devopsd start
   ```
   By default, `devopsd` listens on port 10101.
   See the [system configuration](#system-configuration) section below for how to set the port.

4. Configure the [webhook on Gitlab](https://gitlab.com/help/user/project/integrations/webhooks) for the project you would like to deploy.
Provide the URL to the server where `devopsd` is installed and listening.
(It is strongly recommended that a [secret token](https://gitlab.com/help/user/project/integrations/webhooks#secret-token) is used to ensure the webhook requests are legitimate.)

### System Configuration

`devopsd` is a program that manages the building and deploying of websites from source.
Like many other programs, `devopsd` has a "system configuration" file that contains information about how the program should run on its server.
A sample system configuration file is as follows:

```yaml
host: localhost # required
port: 10101     # required

gitlabToken: ffca05f9 # required, this is a test token
runnerCount: 1        # optional, 1 is default

caddyRoot: /usr/local/www          # required
caddyReload: service caddy restart # required
```

### Project Configuration

Any project that is managed by `devopsd` must contain a file that has the necessary information about how to build and deploy it. This is referred to as a "project configuration" file.
A sample project configuration file is as follows.
All fields seen below are required:

```yaml
domain: steverusso.tech
build:
  cmds:
    - yarn
    - yarn build
  dir: dist
```

## Testing

See the [testing docs](testdata/).

## Unlicense

This is free and unencumbered software released into the public domain. See `UNLICENSE` file.
For more information, please refer to [unlicense.org](http://unlicense.org/).
