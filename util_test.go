// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

import (
	"testing"
)

// Test that we can run a system command as a string.
func TestCommandf(t *testing.T) {
	testCmd, _ := Commandf("printf %s", "hi")
	output, err := testCmd.Output()
	if err != nil {
		panic(err)
	}
	if string(output) != "hi" {
		t.Errorf(`output doesn't match: got "%v", wanted "hi"`, string(output))
	}
}

// Test the FullDomain function by testing different project configurations
// and branch names.
func TestFullDomain(t *testing.T) {
	// The test configurations.
	testProjConfs := []ProjectConfig{
		ProjectConfig{
			TopLevelDomain:  "domain.com",
			MasterSubdomain: "testvalue",
		},
		ProjectConfig{
			TopLevelDomain:  "domain.com",
			MasterSubdomain: "www",
		},
		ProjectConfig{
			TopLevelDomain: "domain.com",
		},
	}

	// The names of the branches to test...
	testBranches := []string{"master", "master", "branch1"}

	// ...and the domains that we expect to get for them.
	expected := []string{"testvalue.domain.com", "www.domain.com", "branch1.domain.com"}

	// Go through the project configurations and get a full domain for each one
	// using the corresponding branch name.
	for i, testConf := range testProjConfs {
		fullDomain := FullDomain(testBranches[i], &testConf)
		if fullDomain != expected[i] {
			t.Errorf("util: FullDomain: got %v, wanted %v", fullDomain, expected)
		}
	}
}
