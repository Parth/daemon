module devopsd

go 1.12

require (
	github.com/gorilla/mux v1.7.1
	github.com/mattn/go-sqlite3 v1.10.0
	gopkg.in/yaml.v3 v3.0.0-20190409140830-cdc409dda467
)
