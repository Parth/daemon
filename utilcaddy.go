// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

import (
	"fmt"
	"os"
	"path/filepath"
	"text/template"
)

// Caddyfile site configuration template.
var t = template.Must(template.New("caddyFileTemp").Parse(`
{{.Site}} {
	root {{.Root}}
	{{.Extras}}
}
`))

// WriteCaddyFile takes the full domain of a site and the path representing
// Caddy's root directory and creates a Caddyfile for that site named
// `<fullDomain>.Caddyfile`. Returns the path of the file it wrote to.
func WriteCaddyFile(caddyRoot, fullDomain, extras string) (string, error) {
	// The configs that we will add to the Caddyfile
	caddyFileConfigs := struct {
		Site   string
		Root   string // The site 'root' (the directory with all the files to serve).
		Extras string
	}{
		Site:   fullDomain,
		Root:   filepath.Join(caddyRoot, fullDomain),
		Extras: extras,
	}

	// Path of the Caddyfile we want to create.
	caddyFilePath := caddyFileConfigs.Root + ".Caddyfile"

	// Open the Caddyfile. This will overwrite anything there.
	caddyFile, err := os.Create(caddyFilePath)
	if err != nil {
		return caddyFilePath, err
	}
	defer caddyFile.Close()

	// Execute the template with the Caddy configs struct values.
	return caddyFilePath, t.Execute(caddyFile, caddyFileConfigs)
}

// ReloadCaddy runs a system command to restart Caddy. The exact system
// command is a configuration value from the system config file.
func ReloadCaddy() error {
	// The system command from the system configuration file.
	rCaddy := CommandStr("sudo kill -USR1 `cat /var/run/caddy.pid`")
	if err := rCaddy.Run(); err != nil {
		return fmt.Errorf("caddy reload: %v", err)
	}

	// Command ended with exit status 0.
	return nil
}
