// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

import (
	"path"
)

type Job struct {
	Id     int64 `json:"id"`
	Status int   `json:"status"`

	Kind    string `json:"kind"`
	RepoUrl string `json:"repo_url"`

	Before      string `json:"before"`
	After       string `json:"after"`
	CheckoutSha string `json:"checkout_sha"`
	Ref         string `json:"ref"`
}

// isZeroedString returns true if the After field is a string of only zeros.
func (j *Job) isZeroedString() bool {
	for _, r := range j.After {
		if r != '0' {
			return false
		}
	}
	return true
}

// IsClosingPush returns true if both:
//  - the CheckoutSha field is an empty string, and
//  - the After field is a string of zeroes.
// These together indicate that the webhook trigger was likely caused
// by a branch being deleted.
func (j *Job) IsClosingPush() bool {
	return j.isZeroedString() && j.CheckoutSha == ""
}

// TargetCommitHash returns the appropriate commit hash for the Job.
// If it's a webhook triggered by a branch being deleted, then the `After`
// field will be zeroed and the CheckoutSha will be empty. The fallback
// in these cases is the `Before` field.
func (j *Job) TargetCommitHash() string {
	if j.IsClosingPush() {
		return j.Before
	}
	return j.CheckoutSha
}

// BranchName returns the branch name based on the 'ref' field provided
// by the webhook (usually in 'refs/heads/<branch_name>' format).
func (j *Job) BranchName() string {
	return path.Base(j.Ref)
}
