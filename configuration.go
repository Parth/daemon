// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type Configuration struct {
	// usageInfo stores what configurations can be set either from the command
	// line and configuration file. Certain values can only be set via command
	// line argument; the rest can be set via either cli or configuration file.
	usageInfo struct {
		cliOnly map[string][]string
		configs map[string][]string
	}

	// values stores the configuration values read in from the command line
	// and configuration file. Certain values can only be set via command line
	// argument; the rest can be set via either cli or configuration file.
	valuesFrom struct {
		cliArgs    map[string]string
		configFile map[string]string
		defaults   map[string]string
	}
}

// NewConfiguration is the constructor function for Configuration.
func NewConfiguration() *Configuration {
	c := &Configuration{}

	c.usageInfo.cliOnly = make(map[string][]string)
	c.usageInfo.configs = make(map[string][]string)

	c.valuesFrom.cliArgs = make(map[string]string)
	c.valuesFrom.configFile = make(map[string]string)
	c.valuesFrom.defaults = make(map[string]string)

	// Configuration values that can only be set via command line arguments.
	c.Info("--help", "", "Display this usage message.")
	c.Info("--config", "<filepath>", "The path to the config file.")
	c.Info("--datadir", "<filepath>", "Path to directory containing config file, db file, etc.", "/usr/local/devopsd/")

	// Configuration values that can be set via either cli or configuration file.
	c.Info("port", "<port#>", "The port number to listen on.")
	c.Info("host", "<hostname>", "The host to listen on.")
	c.Info("gitlab-token", "<token>", "The authentication token used for validating requests.")
	c.Info("runner-count", "<#runners>", "The number of runners to start.", "2")
	c.Info("caddy-root", "<filepath>", "Caddy's root directory.", "/usr/local/www/")

	return c
}

// Info sets the usage info for the given configuration values. If the
// configuration's name beings with '--', then it is considered a
// configuration that can only be set via command line argument.
func (c *Configuration) Info(name, exampleVal, desc string, defaultVal ...string) {
	// Assemble the information about the configuration. If a default value was
	// provided, add it to the usage info about the configuration and add to the
	// map of default configuration values.
	otherInfo := []string{exampleVal, desc}
	if len(defaultVal) > 0 {
		otherInfo = append(otherInfo, defaultVal[0])
		c.valuesFrom.defaults[name] = defaultVal[0]
	}

	// If the name begins with '--', it's a command line argument.
	// If not, it's a general configuration setting.
	if strings.HasPrefix(name, "--") {
		c.usageInfo.cliOnly[name[2:]] = otherInfo
	} else {
		c.usageInfo.configs[name] = otherInfo
	}
}

func (c *Configuration) Usage() string {
	// usageStr takes a title and map of usage info data
	// and returns the assembled help message string.
	usageStr := func(title string, data map[string][]string) (str string) {
		// If there is no data, return empty string.
		if len(data) <= 0 {
			return ""
		}

		str += title
		for name, otherInfo := range data {
			if otherInfo[0] != "" {
				name += "=" + otherInfo[0]
			}

			desc := otherInfo[1]
			if len(otherInfo) > 2 {
				desc += fmt.Sprintf(" (default: %v)", otherInfo[2])
			}

			str += fmt.Sprintf("  --%-30s%s\n", name, desc)
		}
		return str
	}

	msg := "\ndevopsd [configs]\n"
	msg += usageStr("\nCommand Line Only:\n", c.usageInfo.cliOnly)
	msg += usageStr("\nCommand Line or Config File:\n", c.usageInfo.configs)
	return msg
}

// IsValidConfig returns true if the given configuration name is set in
// the usage info data of the different types of configuration settings.
func (c *Configuration) IsValidConfig(name string) bool {
	_, isSetInCliOnly := c.usageInfo.cliOnly[name]
	_, isSetInConfigs := c.usageInfo.configs[name]
	return isSetInCliOnly || isSetInConfigs
}

// ParseConfig extracts the configuration name and value from the given string.
// If there is a an '=' character, it returns everything before the first '='
// as the name and everything after the first '=' as the value. If there is no
// '=', it returns s as the name and an empty string as the value.
func (c *Configuration) ParseConfig(s string) (string, string) {
	if eqIndex := strings.IndexRune(s, '='); eqIndex > 0 {
		return s[:eqIndex], s[eqIndex+1:]
	}
	return s, ""
}

// ParseCliArgs processes the command line arguments and applies the settings
// that are found.
func (c *Configuration) ParseCliArgs() error {
	for _, arg := range os.Args[1:] {
		if !strings.HasPrefix(arg, "--") {
			return fmt.Errorf("cli args must start with '--': '%v'", arg)
		}

		// Get the key -> value pairing from the text after the '--' prefix.
		// Return error if the key is not recognized.
		key, val := c.ParseConfig(arg[2:])
		if !c.IsValidConfig(key) {
			return fmt.Errorf("unknown configuration setting: '%v'", key)
		}

		c.valuesFrom.cliArgs[key] = val
	}

	// If the 'help' flag was read, print the usage message and exit without error.
	if _, hasHelpFlag := c.valuesFrom.cliArgs["help"]; hasHelpFlag {
		fmt.Println(c.Usage())
		os.Exit(0)
	}

	return nil
}

func (c *Configuration) LoadConfigFile(path ...string) error {
	var filePath string
	cliVal, isCliArg := c.valuesFrom.cliArgs["config"]
	defVal, hasDefVal := c.valuesFrom.defaults["config"]

	// Determine the path of the configuration file to load.
	switch {
	// If there was an argument passed to this func call, use that.
	case len(path) > 0:
		filePath = path[0]
	// If the configuration file path was set via a cli arg.
	case isCliArg:
		filePath = cliVal
	// If the configuration file path has a default value.
	case hasDefVal:
		filePath = defVal
	// If there is no value for the path to the configuration file.
	default:
		return fmt.Errorf("have no configuration file path")
	}

	// Open the config file. Return on error.
	file, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	// Create a new Scanner for the config file.
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	// Read the file content line by line.
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		if line == "" || strings.HasPrefix(line, "#") {
			continue
		}

		name, value := c.ParseConfig(line)

		// Return error if the configuration name is not recognized.
		if _, isValid := c.usageInfo.configs[name]; !isValid {
			return fmt.Errorf("unknown configuration: '%v'", name)
		}

		// Apply the configuration.
		c.valuesFrom.configFile[name] = value
	}

	// All went well, return no error.
	return nil
}

// Get returns the value of the given configuration. Certain settings take
// precedence over others. The ranking is:
//  1) Command line argument.
//  2) Config file setting.
//  3) Default value.
//  4) Unknown: zero value.
func (c *Configuration) Get(key string) string {
	cliArgVal, isSetInCliArgs := c.valuesFrom.cliArgs[key]
	configVal, isSetInConfigs := c.valuesFrom.configFile[key]
	defaultVal, isSetInDefaults := c.valuesFrom.defaults[key]

	switch {
	case isSetInCliArgs:
		return cliArgVal
	case isSetInConfigs:
		return configVal
	case isSetInDefaults:
		return defaultVal
	default:
		return ""
	}
}

func (c *Configuration) DataDir() string {
	return c.valuesFrom.cliArgs["datadir"]
}
