// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

import (
	"bytes"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/mux"
)

var (
	testRouter *mux.Router
	config     *Configuration
)

func init() {
	config = NewConfiguration()
	if err := config.LoadConfigFile("./testdata/devopsd.conf"); err != nil {
		panic(err)
	}
	testRouter = NewDevopsdRouter(&Environment{Config: config})
}

func TestGlwhBuildAndDeploy(t *testing.T) {
	// Create a [string]io.Reader map to store the POST request bodies (io.Reader's) by name (string).
	glwhReqData := make(map[string]io.Reader)

	// One for JSON that has bad syntax.
	glwhReqData["bad_json"] = bytes.NewReader([]byte(`{noquotes: "value"}`))

	// Get the example POST request data from the testdata JSON files.
	for _, hookName := range []string{"push"} {
		glwhData, err := ioutil.ReadFile("./testdata/gitlab-webhook-" + hookName + ".json")
		if err != nil {
			t.Fatal(err)
		}
		glwhReqData[hookName] = bytes.NewReader(glwhData)
	}

	// Test bad requests
	// We expect these requests to fail for the reasons below.
	testRequest(t, glwhReqData["push"], "bad_token", http.StatusBadRequest, `{"error": "gitlab token mismatch"}`)                    // Wrong token
	testRequest(t, nil, config.Get("gitlab-token"), http.StatusBadRequest, `{"error": "body is nil"}`)                               // Nil body
	testRequest(t, glwhReqData["bad_json"], config.Get("gitlab-token"), http.StatusBadRequest, `{"error": "reading json req data"}`) // Bad JSON

	// Test good requests
	// We expect these requests to go smoothly.
	//testRequest(t, glwhReqData["push"], sysConf.GitlabToken, http.StatusOK, "")          // Push webhook trigger
}

func testRequest(t *testing.T, body io.Reader, token string, expStatus int, expBody string) {
	// The mock GitLab webhook POST request.
	req, err := http.NewRequest("POST", "/glwh-deploy", body)
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("X-Gitlab-Token", token)

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()

	// Service using our testRouter.
	testRouter.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != expStatus {
		t.Errorf("handler returned wrong status code: got %v want %v", status, expStatus)
	}

	// Check the body is what we expect.
	gotBody := strings.TrimSpace(rr.Body.String())
	if gotBody != expBody {
		t.Errorf("handler returned unexpected body: got '%v' want '%v'", gotBody, expBody)
	}
}
