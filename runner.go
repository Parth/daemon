// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

import (
	"log"
)

// A Runner receives Jobs to process.
type Runner struct {
	// An integer identifier.
	Id int

	// stopCh is a signal channel for
	// terminating the Runner's goroutine.
	stopCh chan struct{}
}

// NewRunner is the constructor for a Runner.
// It sets the Runner's ID and creates the signal channel.
func NewRunner(id int) *Runner {
	return &Runner{
		Id:     id,
		stopCh: make(chan struct{}),
	}
}

// Start creates a goroutine that receives Jobs from the given Job channel.
// The goroutine function returns when the signal channel is closed.
func (r *Runner) Start(jobChan chan *Job, caddyRoot string) {
	go func() {
		for {
			select {
			// If the signal channel closed, terminate the goroutine by returning.
			case _, closed := <-r.stopCh:
				if closed {
					return
				}
			// While the signal channel has not closed, listen for Jobs.
			case job := <-jobChan:
				if err := Process(job, caddyRoot); err != nil {
					log.Printf("runner %v, job %v: error: %v", r.Id, job.Id, err)
				}
			}
		}
	}()
}

// Stop closes the Runner's signal channel. When the
// signal channel is closed, its goroutines will end.
func (r *Runner) Stop() {
	close(r.stopCh)
}
