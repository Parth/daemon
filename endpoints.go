// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

func GlwhBuildAndDeploy(env *Environment) func(w http.ResponseWriter, req *http.Request) {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		// Decode POST request JSON data into Webhook object.
		var wh Webhook
		if err := json.NewDecoder(req.Body).Decode(&wh); err != nil {
			http.Error(w, `{"error": "reading json req data"}`, http.StatusBadRequest)
			return
		}

		// Add this Webhook data as a job in the database.
		jid, err := env.Db.AddJob(&wh)
		if err != nil {
			log.Print(err)
		} else {
			log.Printf("job %v added to db", jid)
		}
	})
}

// InfoOverview sends back a JSON formatted response with miscellaneous
// information about this devopsd instance, such as how many runners
// there are and the job history.
func InfoOverview(env *Environment) func(w http.ResponseWriter, req *http.Request) {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		// Get all the jobs from the database.
		jobs, err := env.Db.GetAllJobs()
		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}

		// Create an OverviewInfo object.
		info := &OverviewInfo{
			RunnerCount: env.RunMngr.RunnerCount(),
			Jobs:        jobs,
		}

		// Convert the OverviewInfo to JSON.
		jsonBytes, err := json.Marshal(info)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			log.Print(err)
			return
		}

		// Write the JSON response.
		fmt.Fprintln(w, string(jsonBytes))
	})
}
