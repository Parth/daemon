// This is free and unencumbered software released
// into the public domain. See the UNLICENSE file.
package main

type Environment struct {
	Config  *Configuration
	Db      *Database
	RunMngr *RunnerManager
}
